﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamTrainer
{
    public partial class QuestionCreator : Form
    {
        public bool RequestCreation = false;

        public QuestionCreator()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double weight;
            bool validWeight = double.TryParse(QuestionWeight.Text,out weight);

            if (!validWeight)
            {
                QuestionWeight.BackColor = Color.Red;
            }
            else
            {
                RequestCreation = true;
                this.Close();
            }

        }

        public Question CreateQuestion()
        {
            Question qst = new Question();
            qst.QuestionHasImage = QuestionCB.Checked;
            qst.AnswerHasImage = AnswerCB.Checked;
            qst.QuestionImagePath = QuestionImagePath.Text;
            qst.QuestionText = QuestionText.Text;
            qst.AnswerText = AnswerText.Text;
            qst.AnswerImagePath = AnswerImagePath.Text;
            qst.Identifier = "qst_"+CalculateMD5Hash(qst.QuestionText);
            try
            {
                qst.Weight = double.Parse(QuestionWeight.Text);
            }
            catch (Exception)
            {
                qst.Weight = 1;
            }
            return qst;
        }

        public string CalculateMD5Hash(string input)
        {

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }


    }
}
