﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hoepp.XmlLib;
using System.Xml.Linq;
using System.IO;
using ExamTrainer.QuestionPickers;

namespace ExamTrainer
{
    public partial class ExamTrainerForm : Form
    {
        public QuestionPicker Picker = new WeightedQuestionPicker();

        private List<Question> _questionList;
        private Question _loadedQuestion = null;
        private Dictionary<String, PlayerStats> _questionStats = new Dictionary<string, PlayerStats>();
        private bool _questionsChanged = false;

        public ExamTrainerForm()
        {
            InitializeComponent();

            XClass.InitCulture();

            this.FormClosing += ExamTrainerForm_FormClosing;

            AnswerButton.Click += AnswerButton_Click;
            AnswerCorrectButton.Click += AnswerCorrectButton_Click;
            AnswerWrongButton.Click += AnswerWrongButton_Click;
            AnswerPartiallyCorrectButton.Click += AnswerPartiallyCorrectButton_Click;
            AnswerSkipButton.Click += AnswerSkipButton_Click;
            FlipBackButton.Click += FlipBackButton_Click;

            LoadQuestionList("Questions.xml");
            LoadPlayerStats("PlayerStats.xml");
            if (_questionList.Count > 0)
            {
                NextQuestion();
            }
            else
            {
                //Show some message on screen that nothing could be loaded

            }
        }

        void FlipBackButton_Click(object sender, EventArgs e)
        {
            LoadQuestion(_loadedQuestion);
        }

        private void ExamTrainerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_questionsChanged)
            {
                if (DialogResult.No == MessageBox.Show(this, "There are unsaved Questions, which will be lost. Are you sure you want to close?", "Confirm closing", MessageBoxButtons.YesNo))
                {
                    e.Cancel = true;
                }
            }
        }

        public void QuestionCreation()
        {
            QuestionCreator creatorForm = new QuestionCreator();
            creatorForm.ShowDialog();
            if (creatorForm.RequestCreation)
            {
                _questionList.Add(creatorForm.CreateQuestion());
                this._questionsChanged = true;
            }
            creatorForm.Dispose();
            
        }

        void AnswerSkipButton_Click(object sender, EventArgs e)
        {
            NextQuestion();
        }

        private void AnswerWrongButton_Click(object sender, EventArgs e)
        {
            PlayerStats ps = GetCurrentPlayerStats();
            ps.Fails++;
            ps.Tries++;
            SavePlayerStats();

            NextQuestion();
        }

        void AnswerPartiallyCorrectButton_Click(object sender, EventArgs e)
        {
            PlayerStats ps = GetCurrentPlayerStats();
            ps.PartialSuccesses++;
            ps.Tries++;
            SavePlayerStats();

            NextQuestion();
        }

        void AnswerCorrectButton_Click(object sender, EventArgs e)
        {
            PlayerStats ps = GetCurrentPlayerStats();
            ps.Successes++;
            ps.Tries++;
            SavePlayerStats();

            NextQuestion();
        }

        public void SavePlayerStats()
        {
            XElement root = new XElement("Stats");
            foreach(PlayerStats stat in _questionStats.Values)
            {
                root.Add(XClass.Serialize(stat));
            }
            root.Save("PlayerStats.xml");
        }

        public void SaveQuestions()
        {
            XElement root = new XElement("Questions");
            foreach (Question qst in _questionList)
            {
                root.Add(XClass.Serialize(qst));
            }
            root.Save("Questions.xml");
            _questionsChanged = false;
        }


        public PlayerStats GetCurrentPlayerStats()
        {
            string identifier = _loadedQuestion.Identifier;
            if (_questionStats.ContainsKey(identifier))
            {
                return _questionStats[identifier];
            }
            else
            {
                PlayerStats ps = new PlayerStats();
                ps.QuestionIdentifier = identifier;
                _questionStats.Add(identifier,ps);
                return ps;
            }
        }

        public void NextQuestion()
        {
            Question qst = Picker.PickQuestion(_questionList,_questionStats);
            LoadQuestion(qst);
        }

        public void LoadPlayerStats(string path)
        {
            if (!File.Exists(path))
            {
                _questionStats = new Dictionary<string,PlayerStats>();
                return;
            }
            XElement root = XDocument.Load(path).Root;
            List<XElement> XElements = root.Elements().ToList();

            _questionStats = new Dictionary<string, PlayerStats>();

            foreach (XElement elem in XElements)
            {
                PlayerStats stat = (PlayerStats)XDeserializer.Deserialize(elem, typeof(PlayerStats));
                _questionStats.Add(stat.QuestionIdentifier, stat);
            }
        }

        public void LoadQuestionList(string path)
        {
            if(!File.Exists(path))
            {
                _questionList = new List<Question>();
                return;
            }
            XElement root = XDocument.Load(path).Root;
            List<XElement> XElements = root.Elements().ToList();

            _questionList = new List<Question>();

            foreach (XElement elem in XElements)
            {
                Question qst = (Question)XDeserializer.Deserialize(elem,typeof(Question));
                _questionList.Add(qst);
            }

        }

        public void AnswerButton_Click(object sender, EventArgs e)
        {

            if (_loadedQuestion.AnswerHasImage)
            {
                this.TextBoxBig.Visible = false;
                this.TextBoxSmall.Visible = true;
                this.ImageBox.Visible = true;
                this.TextBoxSmall.Text = _loadedQuestion.AnswerText;
                try
                {
                    this.ImageBox.Image = Image.FromFile(_loadedQuestion.AnswerImagePath);
                }
                catch (Exception)
                {
                    MessageBox.Show("Error while loading image");
                }
            }
            else
            {
                this.TextBoxBig.Visible = true;
                this.TextBoxSmall.Visible = false;
                this.ImageBox.Visible = false;
                this.TextBoxBig.Text = _loadedQuestion.AnswerText;
            }

            this.AnswerPanel.Visible = true;
            this.AnswerButton.Visible = false;

        }

        public void LoadQuestion( Question qst )
        {

            if (qst.QuestionHasImage)
            {
                this.TextBoxBig.Visible = false;
                this.TextBoxSmall.Visible = true;
                this.ImageBox.Visible = true;
                this.TextBoxSmall.Text = qst.QuestionText;
                try
                {
                    this.ImageBox.Image = Image.FromFile(qst.QuestionImagePath);
                }
                catch (Exception)
                {
                    MessageBox.Show("Error while loading image");
                }
            }
            else
            {
                this.TextBoxBig.Visible = true;
                this.TextBoxSmall.Visible = false;
                this.ImageBox.Visible = false;
                this.TextBoxBig.Text = qst.QuestionText;
            }

            this.AnswerPanel.Visible = false;
            this.AnswerButton.Visible = true;

            _loadedQuestion = qst;
        }

        private void saveConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SaveQuestions();
            this.SavePlayerStats();
        }

        private void loadConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LoadQuestionList("Questions.xml");
            this.LoadPlayerStats("PlayerStats.xml");
            if (_questionList.Count > 0)
            {
                NextQuestion();
            }
        }

        private void addQuestionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.QuestionCreation();
        }


        

    }
}
