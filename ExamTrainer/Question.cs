﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hoepp.XmlLib;

namespace ExamTrainer
{
    [XClass("Question",typeof(Question))]
    public class Question
    {
        [XObject("Identifier")]
        public string Identifier;

        [XObject("QuestionText")]
        public string QuestionText;
        [XObject("AnswerText")]
        public string AnswerText;

        [XObject("AnswerHasImage")]
        public bool AnswerHasImage;
        [XObject("QuestionHasImage")]
        public bool QuestionHasImage;

        [XObject("AnswerImagePath")]
        public string AnswerImagePath;
        [XObject("QuestionImagePath")]
        public string QuestionImagePath;

        [XObject("Weight")]
        public double Weight;

    }
}
