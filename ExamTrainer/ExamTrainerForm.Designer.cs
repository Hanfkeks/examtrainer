﻿namespace ExamTrainer
{
    partial class ExamTrainerForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExamTrainerForm));
            this.TextBoxBig = new System.Windows.Forms.RichTextBox();
            this.TextBoxSmall = new System.Windows.Forms.RichTextBox();
            this.AnswerPanel = new System.Windows.Forms.Panel();
            this.FlipBackButton = new System.Windows.Forms.Button();
            this.AnswerCorrectButton = new System.Windows.Forms.Button();
            this.AnswerSkipButton = new System.Windows.Forms.Button();
            this.AnswerPartiallyCorrectButton = new System.Windows.Forms.Button();
            this.AnswerWrongButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ExamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addQuestionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageBox = new System.Windows.Forms.PictureBox();
            this.AnswerButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.AnswerPanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxBig
            // 
            this.TextBoxBig.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxBig.Location = new System.Drawing.Point(13, 36);
            this.TextBoxBig.Name = "TextBoxBig";
            this.TextBoxBig.ReadOnly = true;
            this.TextBoxBig.Size = new System.Drawing.Size(1010, 632);
            this.TextBoxBig.TabIndex = 1;
            this.TextBoxBig.Text = "Fragetext";
            // 
            // TextBoxSmall
            // 
            this.TextBoxSmall.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxSmall.Location = new System.Drawing.Point(13, 36);
            this.TextBoxSmall.Name = "TextBoxSmall";
            this.TextBoxSmall.ReadOnly = true;
            this.TextBoxSmall.Size = new System.Drawing.Size(1010, 196);
            this.TextBoxSmall.TabIndex = 2;
            this.TextBoxSmall.Text = "";
            // 
            // AnswerPanel
            // 
            this.AnswerPanel.Controls.Add(this.FlipBackButton);
            this.AnswerPanel.Controls.Add(this.AnswerCorrectButton);
            this.AnswerPanel.Controls.Add(this.AnswerSkipButton);
            this.AnswerPanel.Controls.Add(this.AnswerPartiallyCorrectButton);
            this.AnswerPanel.Controls.Add(this.AnswerWrongButton);
            this.AnswerPanel.Location = new System.Drawing.Point(228, 674);
            this.AnswerPanel.Name = "AnswerPanel";
            this.AnswerPanel.Size = new System.Drawing.Size(572, 100);
            this.AnswerPanel.TabIndex = 8;
            // 
            // FlipBackButton
            // 
            this.FlipBackButton.BackgroundImage = global::ExamTrainer.Properties.Resources.FlipIcon;
            this.FlipBackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.FlipBackButton.Location = new System.Drawing.Point(3, -1);
            this.FlipBackButton.Name = "FlipBackButton";
            this.FlipBackButton.Size = new System.Drawing.Size(75, 39);
            this.FlipBackButton.TabIndex = 10;
            this.toolTip1.SetToolTip(this.FlipBackButton, "Flip back to Question");
            this.FlipBackButton.UseVisualStyleBackColor = true;
            // 
            // AnswerCorrectButton
            // 
            this.AnswerCorrectButton.BackgroundImage = global::ExamTrainer.Properties.Resources.SuccessIcon;
            this.AnswerCorrectButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AnswerCorrectButton.Location = new System.Drawing.Point(191, 0);
            this.AnswerCorrectButton.Name = "AnswerCorrectButton";
            this.AnswerCorrectButton.Size = new System.Drawing.Size(75, 38);
            this.AnswerCorrectButton.TabIndex = 4;
            this.toolTip1.SetToolTip(this.AnswerCorrectButton, "Correct Answer");
            this.AnswerCorrectButton.UseVisualStyleBackColor = true;
            // 
            // AnswerSkipButton
            // 
            this.AnswerSkipButton.BackgroundImage = global::ExamTrainer.Properties.Resources.SkipIcon;
            this.AnswerSkipButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AnswerSkipButton.Location = new System.Drawing.Point(494, -1);
            this.AnswerSkipButton.Name = "AnswerSkipButton";
            this.AnswerSkipButton.Size = new System.Drawing.Size(75, 38);
            this.AnswerSkipButton.TabIndex = 7;
            this.toolTip1.SetToolTip(this.AnswerSkipButton, "Skip question and prevent logging");
            this.AnswerSkipButton.UseVisualStyleBackColor = true;
            // 
            // AnswerPartiallyCorrectButton
            // 
            this.AnswerPartiallyCorrectButton.BackgroundImage = global::ExamTrainer.Properties.Resources.PartiallyCorrectIcon;
            this.AnswerPartiallyCorrectButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AnswerPartiallyCorrectButton.Location = new System.Drawing.Point(272, 0);
            this.AnswerPartiallyCorrectButton.Name = "AnswerPartiallyCorrectButton";
            this.AnswerPartiallyCorrectButton.Size = new System.Drawing.Size(75, 38);
            this.AnswerPartiallyCorrectButton.TabIndex = 5;
            this.toolTip1.SetToolTip(this.AnswerPartiallyCorrectButton, "Partially Correct Answer");
            this.AnswerPartiallyCorrectButton.UseVisualStyleBackColor = true;
            // 
            // AnswerWrongButton
            // 
            this.AnswerWrongButton.BackgroundImage = global::ExamTrainer.Properties.Resources.FailIcon;
            this.AnswerWrongButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AnswerWrongButton.Location = new System.Drawing.Point(353, 0);
            this.AnswerWrongButton.Name = "AnswerWrongButton";
            this.AnswerWrongButton.Size = new System.Drawing.Size(75, 38);
            this.AnswerWrongButton.TabIndex = 6;
            this.toolTip1.SetToolTip(this.AnswerWrongButton, "Incorrect Answer");
            this.AnswerWrongButton.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExamToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1035, 28);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ExamToolStripMenuItem
            // 
            this.ExamToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addQuestionToolStripMenuItem,
            this.saveConfigurationToolStripMenuItem,
            this.loadConfigurationToolStripMenuItem});
            this.ExamToolStripMenuItem.Name = "ExamToolStripMenuItem";
            this.ExamToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.ExamToolStripMenuItem.Text = "Exam";
            // 
            // addQuestionToolStripMenuItem
            // 
            this.addQuestionToolStripMenuItem.Name = "addQuestionToolStripMenuItem";
            this.addQuestionToolStripMenuItem.Size = new System.Drawing.Size(212, 26);
            this.addQuestionToolStripMenuItem.Text = "Add Question";
            this.addQuestionToolStripMenuItem.Click += new System.EventHandler(this.addQuestionToolStripMenuItem_Click);
            // 
            // saveConfigurationToolStripMenuItem
            // 
            this.saveConfigurationToolStripMenuItem.Name = "saveConfigurationToolStripMenuItem";
            this.saveConfigurationToolStripMenuItem.Size = new System.Drawing.Size(212, 26);
            this.saveConfigurationToolStripMenuItem.Text = "Save Configuration";
            this.saveConfigurationToolStripMenuItem.Click += new System.EventHandler(this.saveConfigurationToolStripMenuItem_Click);
            // 
            // loadConfigurationToolStripMenuItem
            // 
            this.loadConfigurationToolStripMenuItem.Name = "loadConfigurationToolStripMenuItem";
            this.loadConfigurationToolStripMenuItem.Size = new System.Drawing.Size(212, 26);
            this.loadConfigurationToolStripMenuItem.Text = "Load Configuration";
            this.loadConfigurationToolStripMenuItem.Click += new System.EventHandler(this.loadConfigurationToolStripMenuItem_Click);
            // 
            // ImageBox
            // 
            this.ImageBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ImageBox.Location = new System.Drawing.Point(13, 238);
            this.ImageBox.Name = "ImageBox";
            this.ImageBox.Size = new System.Drawing.Size(1011, 430);
            this.ImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImageBox.TabIndex = 3;
            this.ImageBox.TabStop = false;
            // 
            // AnswerButton
            // 
            this.AnswerButton.BackgroundImage = global::ExamTrainer.Properties.Resources.FlipIcon;
            this.AnswerButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AnswerButton.Location = new System.Drawing.Point(479, 674);
            this.AnswerButton.Name = "AnswerButton";
            this.AnswerButton.Size = new System.Drawing.Size(75, 39);
            this.AnswerButton.TabIndex = 0;
            this.toolTip1.SetToolTip(this.AnswerButton, "Flip to Answer");
            this.AnswerButton.UseVisualStyleBackColor = true;
            // 
            // ExamTrainerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 718);
            this.Controls.Add(this.AnswerPanel);
            this.Controls.Add(this.ImageBox);
            this.Controls.Add(this.TextBoxSmall);
            this.Controls.Add(this.TextBoxBig);
            this.Controls.Add(this.AnswerButton);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ExamTrainerForm";
            this.Text = "Exam Trainer";
            this.AnswerPanel.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AnswerButton;
        private System.Windows.Forms.RichTextBox TextBoxBig;
        private System.Windows.Forms.RichTextBox TextBoxSmall;
        private System.Windows.Forms.Button AnswerCorrectButton;
        private System.Windows.Forms.Button AnswerPartiallyCorrectButton;
        private System.Windows.Forms.Button AnswerWrongButton;
        private System.Windows.Forms.Button AnswerSkipButton;
        private System.Windows.Forms.Panel AnswerPanel;
        private System.Windows.Forms.PictureBox ImageBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ExamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addQuestionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadConfigurationToolStripMenuItem;
        private System.Windows.Forms.Button FlipBackButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

