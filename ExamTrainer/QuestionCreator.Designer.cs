﻿namespace ExamTrainer
{
    partial class QuestionCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionCreator));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.QuestionText = new System.Windows.Forms.TextBox();
            this.QuestionImagePath = new System.Windows.Forms.TextBox();
            this.QuestionCB = new System.Windows.Forms.CheckBox();
            this.AnswerCB = new System.Windows.Forms.CheckBox();
            this.AnswerImagePath = new System.Windows.Forms.TextBox();
            this.AnswerText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.QuestionWeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(258, 329);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Question Text";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Question Image Path";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Question Has Image";
            // 
            // QuestionText
            // 
            this.QuestionText.Location = new System.Drawing.Point(215, 10);
            this.QuestionText.Name = "QuestionText";
            this.QuestionText.Size = new System.Drawing.Size(390, 22);
            this.QuestionText.TabIndex = 4;
            // 
            // QuestionImagePath
            // 
            this.QuestionImagePath.Location = new System.Drawing.Point(215, 66);
            this.QuestionImagePath.Name = "QuestionImagePath";
            this.QuestionImagePath.Size = new System.Drawing.Size(390, 22);
            this.QuestionImagePath.TabIndex = 5;
            // 
            // QuestionCB
            // 
            this.QuestionCB.AutoSize = true;
            this.QuestionCB.Location = new System.Drawing.Point(215, 39);
            this.QuestionCB.Name = "QuestionCB";
            this.QuestionCB.Size = new System.Drawing.Size(18, 17);
            this.QuestionCB.TabIndex = 6;
            this.QuestionCB.UseVisualStyleBackColor = true;
            // 
            // AnswerCB
            // 
            this.AnswerCB.AutoSize = true;
            this.AnswerCB.Location = new System.Drawing.Point(215, 232);
            this.AnswerCB.Name = "AnswerCB";
            this.AnswerCB.Size = new System.Drawing.Size(18, 17);
            this.AnswerCB.TabIndex = 12;
            this.AnswerCB.UseVisualStyleBackColor = true;
            // 
            // AnswerImagePath
            // 
            this.AnswerImagePath.Location = new System.Drawing.Point(215, 259);
            this.AnswerImagePath.Name = "AnswerImagePath";
            this.AnswerImagePath.Size = new System.Drawing.Size(390, 22);
            this.AnswerImagePath.TabIndex = 11;
            // 
            // AnswerText
            // 
            this.AnswerText.Location = new System.Drawing.Point(215, 108);
            this.AnswerText.Multiline = true;
            this.AnswerText.Name = "AnswerText";
            this.AnswerText.Size = new System.Drawing.Size(390, 118);
            this.AnswerText.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Answer Has Image";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 262);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Answer Image Path";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Answer Text";
            // 
            // QuestionWeight
            // 
            this.QuestionWeight.Location = new System.Drawing.Point(215, 301);
            this.QuestionWeight.Name = "QuestionWeight";
            this.QuestionWeight.Size = new System.Drawing.Size(74, 22);
            this.QuestionWeight.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Question Weight";
            // 
            // QuestionCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 360);
            this.Controls.Add(this.QuestionWeight);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AnswerCB);
            this.Controls.Add(this.AnswerImagePath);
            this.Controls.Add(this.AnswerText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.QuestionCB);
            this.Controls.Add(this.QuestionImagePath);
            this.Controls.Add(this.QuestionText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "QuestionCreator";
            this.Text = "New Question";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox QuestionText;
        private System.Windows.Forms.TextBox QuestionImagePath;
        private System.Windows.Forms.CheckBox QuestionCB;
        private System.Windows.Forms.CheckBox AnswerCB;
        private System.Windows.Forms.TextBox AnswerImagePath;
        private System.Windows.Forms.TextBox AnswerText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox QuestionWeight;
        private System.Windows.Forms.Label label7;
    }
}