﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTrainer
{
    public interface QuestionPicker
    {

        Question PickQuestion(List<Question> questions, Dictionary<string,PlayerStats> stats);

    }
}
