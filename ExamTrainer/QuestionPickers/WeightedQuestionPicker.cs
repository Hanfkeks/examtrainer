﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTrainer.QuestionPickers
{
    public class WeightedQuestionPicker : QuestionPicker
    {
        public Random Randomizer = new Random();

        public Question PickQuestion(List<Question> questions, Dictionary<string, PlayerStats> stats)
        {
            List<KeyValuePair<object, double>> ratedQuestions = new List<KeyValuePair<object, double>>();
            foreach (Question qst in questions)
            {
                double rating = 1;
                if (stats.ContainsKey(qst.Identifier))
                {
                    PlayerStats stat = stats[qst.Identifier];
                    rating += stat.Successes;
                    rating += stat.PartialSuccesses / 2;
                }
                double randomizationWeight = qst.Weight / rating;
                ratedQuestions.Add(new KeyValuePair<object, double>(qst, randomizationWeight));
            }
            Question result = (Question)WeightedRandom(false, ratedQuestions.ToArray());
            return result;
        }

        public object WeightedRandom(bool output, params KeyValuePair<object, double>[] pairs)
        {
            double sum = 0;
            foreach (var kvp in pairs)
            {
                if (kvp.Value < 0)
                {
                    continue;
                }
                sum += kvp.Value;
            }
            if (output)
            {
                foreach (var kvp in pairs)
                {
                    Console.WriteLine("Chance for " + kvp.Key.ToString() + " = " + ((kvp.Value / sum) * 100).ToString() + "%");
                }
            }
            double rand = Randomizer.NextDouble() * sum;
            double currentSum = 0;
            foreach (var kvp in pairs)
            {
                if (kvp.Value < 0)
                {
                    continue;
                }
                currentSum += kvp.Value;
                if (currentSum > rand)
                {
                    return kvp.Key;
                }
            }
            return pairs.Last().Key;
        }

    }
}

