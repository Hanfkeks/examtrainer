﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTrainer.QuestionPickers
{
    public class RandomQuestionPicker:QuestionPicker
    {
        private Random _rand = new Random();

        public Question PickQuestion(List<Question> questions, Dictionary<string, PlayerStats> stats)
        {
            int index = _rand.Next(0, questions.Count);
            return questions[index];
        }


    }
}
