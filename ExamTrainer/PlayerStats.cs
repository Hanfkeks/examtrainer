﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hoepp.XmlLib;

namespace ExamTrainer
{
    /// <summary>
    /// Player Statistics for one question
    /// </summary>
    [XClass("PlayerStats", typeof(PlayerStats))]
    public class PlayerStats
    {
        [XObject("QuestionIdentifier")]
        public string QuestionIdentifier;
        /// <summary>
        /// how many times did the question appear and was not skipped?
        /// </summary>
        [XObject("Tries")]
        public int Tries = 0;

        [XObject("Fails")]
        public int Fails = 0;
        [XObject("PartialSuccesses")]
        public int PartialSuccesses = 0;
        [XObject("Successes")]
        public int Successes = 0;
    }
}
